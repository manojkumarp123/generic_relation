from django.db import models
from django.contrib.contenttypes.fields import (
    GenericForeignKey, GenericRelation)
from django.contrib.contenttypes.models import ContentType
from django.dispatch import receiver
from django.db.models.signals import pre_delete


class Test(models.Model):
    questions = GenericRelation('Question')


@receiver(pre_delete, sender=Test)
def my_callback(sender, instance, **kwargs):
    print(instance)


class Passage(models.Model):
    test = models.ForeignKey(
        Test, related_name="passages",
        on_delete=models.SET_NULL,
        blank=True, null=True)
    questions = GenericRelation('Question')


class Question(models.Model):
    # Question either belongs to Test or Passage
    content_type = models.ForeignKey(
        ContentType, blank=True, null=True,
        on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(
        blank=True, null=True)
    parent = GenericForeignKey()
