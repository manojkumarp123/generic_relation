from django.test import TestCase
from .models import Test, Passage, Question
from django.contrib.contenttypes.models import ContentType


class ContentTestCase(TestCase):
    def setUp(self):
        t = Test.objects.create()
        p = Passage.objects.create(test=t)

        content_type_test = ContentType.objects.get_for_model(Test)

        Question.objects.create(
            content_type=content_type_test,
            object_id=t.id
        )

        content_type_passage = ContentType.objects.get_for_model(Passage)

        Question.objects.create(
            content_type=content_type_passage,
            object_id=p.id
        )

    def test_delete_without_cascade(self):
        Test.objects.all().delete()
        self.assertEqual(Question.objects.count(), 2)
        self.assertEqual(Passage.objects.count(), 1)
